package com.soportelibre;

/**
 * Clase utilitaria para almacenar el username mediante una variable
 * ThreadLocal.
 *
 * @author Ricardo Arguello <ricardo.arguello@soportelibre.com>
 */
public class OracleAuditUtil {

    private static final ThreadLocal<String> threadUsername = new ThreadLocal<String>();

    public static void setUsername(String username) {
        threadUsername.set(username);
    }

    public static String getUsername() {
        return threadUsername.get();
    }

    public static void removeUsername() {
        threadUsername.remove();
    }
}
