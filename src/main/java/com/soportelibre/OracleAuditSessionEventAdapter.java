package com.soportelibre;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.persistence.exceptions.DatabaseException;
import org.eclipse.persistence.internal.databaseaccess.DatabaseAccessor;
import org.eclipse.persistence.sessions.SessionEvent;
import org.eclipse.persistence.sessions.SessionEventAdapter;

/**
 * Implementacion de SessionEventAdapter de Eclipse Link para intereceptar una
 * conexion de base de datos antes de ser entregada desde un pool de conexiones.
 * Permite implementar auditoria de base de datos mediante la ejecución de
 * DBMS_SESSION.SET_IDENTIFIER.
 *
 * @author Ricardo Arguello <ricardo.arguello@soportelibre.com>
 */
public class OracleAuditSessionEventAdapter extends SessionEventAdapter {

    private static final Logger logger = Logger.getLogger(OracleAuditSessionEventAdapter.class.getName());

    @Override
    public void postAcquireConnection(SessionEvent event) {
        String username = OracleAuditUtil.getUsername();
        logger.info("postAcquireConnection(): username=" + username);

        if (username != null) {
            DatabaseAccessor accessor = (DatabaseAccessor) event.getResult();

            try (Statement stmt = accessor.getConnection().createStatement()) {
                // FIXME: Use PreparedStatement!
                String sql = "BEGIN DBMS_SESSION.SET_IDENTIFIER('" + username + "'); END;";
                stmt.execute(sql);
            } catch (DatabaseException e) {
                logger.log(Level.SEVERE, "DatabaseException al ejecutar DBMS_SESSION.SET_IDENTIFIER", e);
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "SQLException al ejecutar DBMS_SESSION.SET_IDENTIFIER", e);
            }
        }
    }

    @Override
    public void preReleaseConnection(SessionEvent event) {
        String username = OracleAuditUtil.getUsername();
        logger.info("preReleaseConnection(): username=" + username);

        if (username != null) {
            DatabaseAccessor accessor = (DatabaseAccessor) event.getResult();

            try (Statement stmt = accessor.getConnection().createStatement()) {
                String sql = "BEGIN DBMS_SESSION.CLEAR_IDENTIFIER; END;";
                stmt.execute(sql);
            } catch (DatabaseException e) {
                logger.log(Level.SEVERE, "DatabaseException al ejecutar DBMS_SESSION.CLEAR_IDENTIFIER", e);
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "SQLException al ejecutar DBMS_SESSION.CLEAR_IDENTIFIER", e);
            }
        }
    }
}
