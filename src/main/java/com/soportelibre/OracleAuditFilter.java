package com.soportelibre;

import java.io.IOException;
import java.security.Principal;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * Filtro de Servlet para establecer el nombre de usuario (username) en una
 * variable ThreadLocal. Esto permite obtenerlo en la capa de persistencia,
 * mediante el objeto OracleAuditSessionEventAdapter.
 *
 * @author Ricardo Arguello <ricardo.arguello@soportelibre.com>
 */
public class OracleAuditFilter implements Filter {

    private static final Logger logger = Logger.getLogger(OracleAuditFilter.class.getName());

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws ServletException, IOException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        Principal principal = httpRequest.getUserPrincipal();

        if (principal != null) {
            String username = principal.getName();
            logger.info("OracleAuditFilter(): username=" + username);

            OracleAuditUtil.setUsername(username);
            chain.doFilter(request, response);
            OracleAuditUtil.removeUsername();
        }
    }

    @Override
    public void destroy() {
    }
}
